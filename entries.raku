use v6;
use fatal;
use JSON::Fast;
use WWW;

sub download-song-entries-from-vocadb {
    my @songs;
    loop {
        my $uri = "https://vocadb.net/api/songs?tagId%5B%5D=1731\&start={+@songs}\&maxResults=100\&songTypes=Original\&fields=PVs\&lang=Romaji";
        say "Making request to $uri";
        my $response = jget $uri, :User-Agent("Script to fetch entries for fictional language songs for Fickle (https://kyarei.gitlab.io/fickle/) by +merlan #flirora");
        return @songs if !$response<items>;
        @songs.append: $response<items>.Seq;
    }
}

sub get-song-entries-from-vocadb {
    my $cache = 'cached_entries.json'.IO;
    if !$cache.e || $cache.modified < now - Duration.new(3600) {
        my @results = download-song-entries-from-vocadb;
        $cache.spurt(to-json(@results, :pretty));
        @results
    } else {
        from-json $cache.slurp
    }
}

sub has-only-latin-chars($s) {
    $s ~~ / <:Script<Latin>> / &&
        $s ~~ /^ [<:Script<Latin>> | <:Script<Common>>]* $/
}

sub process-string($s) {
    $s.samemark('a')
        .uc
        .subst("'", "", :g)
        .subst(/<:P - [-]>/, " ", :g)
        .subst(/<[ ~ = ]>/, "-", :g)
        .subst(/\s+/, " ", :g)
        .subst("Þ", "TH", :g)
        .subst("Ð", "DH", :g)
        .subst("Ŋ", "NG", :g)
        .trim
}

sub extract-info-from-entry($entry) {
    my $name = $entry{has-only-latin-chars($entry<defaultName>) ?? "defaultName" !! "name"};
    my $key = process-string $name;
    my $deleted = [&&] $entry<pvs>.map({$_<disabled>});
    %(
        id => $entry<id>,
        name => $name,
        origName => $entry<defaultName>,
        key => $key,
        artist => $entry<artistString>,
        extant => !$deleted,
    )
}

my @songs = get-song-entries-from-vocadb;
@songs = @songs.map(&extract-info-from-entry).grep({$_<key> ~~ /^ <:Block('Basic Latin')>+ $/});

'public/entries.json'.IO.spurt(to-json @songs);


