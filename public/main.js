"use strict";

const playArea = document.getElementById("playArea");
const alertBox = document.getElementById("alertMessage");
const inputBox = document.getElementById("inputBox");
const keyboard = document.getElementById("keyboard");
const keyboardButtons = {};

for (let child of keyboard.children) {
    if (child instanceof HTMLButtonElement && child.innerText.length == 1) {
        let t = child.innerText;
        t = t == '␣' ? ' ' : t;
        keyboardButtons[t] = child;
    }
}

const resultsScreen = document.getElementById("resultsScreen");
const resultsScreenTitle = document.getElementById("resultsScreenTitle");
const resultsScreenSubtitle = document.getElementById("resultsScreenSubtitle");
const resultsPlayed = document.getElementById("resultsPlayed");
const resultsGuesses = document.getElementById("resultsGuesses");
const resultsLetters = document.getElementById("resultsLetters");
const resultsAvgGuesses = document.getElementById("resultsAvgGuesses");
const resultsAvgLetters = document.getElementById("resultsAvgLetters");
const resultsTimeToNext = document.getElementById("resultsTimeToNext");
const resultsSongTitle = document.getElementById("resultsSongTitle");
const resultsSongArtist = document.getElementById("resultsSongArtist");
const helpScreen = document.getElementById("helpScreen");
const updateScreen = document.getElementById("updateScreen");

function request(url, listener) {
    let oReq = new XMLHttpRequest();
    oReq.addEventListener("load", listener);
    oReq.open("GET", url);
    oReq.send();
}

function requestp(url) {
    return new Promise((resolve, reject) => {
        let oReq = new XMLHttpRequest();
        oReq.addEventListener("load", function () { resolve(this); });
        oReq.addEventListener("error", function () { reject(this); });
        oReq.open("GET", url);
        oReq.send();
    });
}

let GAME_DATA = null;
let currentGame = null;
let statistics = null;

async function loadGameData() {
    let entries = JSON.parse((await requestp("entries.json")).responseText);
    let validSet = {};
    for (let w of entries) {
        validSet[w.key] = true;
    }
    let secretWords = entries.filter(entry => entry.extant);
    GAME_DATA = {
        validWords: validSet,
        entries: entries,
        secretWords: secretWords,
    };
}

function isValidWord(w) {
    return GAME_DATA.validWords[w] === true;
}

const EPOCH = Date.UTC(2022, 9, 22, 0, 0, 0);
const PERIOD = 24 * 60 * 60 * 1000;
function gameNumber(time) {
    time = time || Date.now();
    return Math.floor((time - EPOCH) / PERIOD);
}

function timeFromGameNumber(n) {
    return EPOCH + PERIOD * n;
}

function seedForGameNumber(n) {
    return 0x6AB91845 + 0x75F * n;
}

function getCurrentGame() {
    let gn = gameNumber();
    let game = localStorage.getItem(`currentGame`);
    if (game !== null) {
        try {
            game = JSON.parse(game);
            if (gn == 259) { // Fix wrong key in previous version
                game.secretWord.key = "ENDE -SHOUNEN GA TSUNEBIKU WA HOUKOU NO GEN-";
            }
        } catch (e) {
            game = null;
        }
    }
    if (game !== null && game.gameNumber == gn) return game;
    let secretWord;
    if (gn == 259) { // Migration from previous version
        secretWord = GAME_DATA.secretWords.find(a => a.name == "Ende =Shounen ga tsunebiku wa houkou no gen-");
    } else {
        let secretWords = GAME_DATA.secretWords;
        let rng = new Math.seedrandom(seedForGameNumber(gn));
        let idx = rng.int32() % secretWords.length;
        if (idx < 0) idx += secretWords.length;
        secretWord = secretWords[idx];
    }
    let priorGuesses = [];
    return {
        gameNumber: gn,
        secretWord: secretWord,
        priorGuesses: priorGuesses,
        status: 'running',
    };
}

function pendingWord() {
    return inputBox.textContent;
}

function setPendingWord(w) {
    inputBox.textContent = w;
}

function loadStatistics() {
    statistics = localStorage.getItem("statistics");
    if (statistics !== null) {
        try {
            statistics = JSON.parse(statistics);
        } catch (e) {
            statistics = null;
        }
    }
    if (statistics === null) {
        statistics = {
            played: 0,
            moves: {},
            totalMoves: 0,
            totalLetters: 0,
        };
    }
}

async function init() {
    console.log("ḋ");
    await loadGameData();
    if (window.localStorage.getItem("KAFU loves you!") === null) {
        window.localStorage.clear();
        window.localStorage.setItem("KAFU loves you!", 1);
    }
    loadStatistics();
    currentGame = getCurrentGame();

    for (let kbButton of Object.values(keyboardButtons)) {
        kbButton.className = "";
    }

    let pg = currentGame.priorGuesses;
    currentGame.priorGuesses = [];
    for (let guess of pg) {
        submitWord(guess, false);
    }

}

function isValidChar(c) {
    return c.match(/^[A-Z]|[0-9]| |-$/);
}

function handleKey(key, elem) {
    if (elem !== undefined) elem.blur();
    if (key === undefined) return;
    if (key == String.fromCharCode(58)) {
        if (elem !== undefined) {
            let p = eval(atob(
                "bmV3IEZ1bmN0aW9uKCJ4IiwgIm4iLCBhdG9iKCJjbVYwZFhKdUlGc3VMaTVCY25KaGVTaDRMbXhsYm1kMGFDa3VhMlY1Y3lncFhTNXRZWEFvYVNBOVBpQjRXMjRnS2lCcElDVWdlQzVzWlc1bmRHaGRLUzVxYjJsdUtDSWlLUT09Iikp"));
            eval(p(atob("d1d6az1WbHcyIiJWKShaIHRsKXBrIG9aKShEIG41VG9nTm9XV2lheCJwbGFUVV9ZUixwRjFvMCkoZTJiUylhSjUoRiJwUTl3STBuPWwpRU1uZFJibGggKDI5YlksYTIzKHgsdGsxcGUsZVNaLmhXZFhW"), 37));
        } else {
            showUpdates();
        }
        return;
    }
    if (currentGame === null || currentGame.status !== 'running') return;
    let pw = pendingWord();
    if (key == "Enter") {
        // submit
        if (pw == "CBAT") {
            window.open("https://youtu.be/KAwyWkksXuo", "_blank");
        }
        if (pw == currentGame.secretWord.key || isValidWord(pw)) {
            submitWord(pw, true);
        } else {
            showAlert(`What the ResourceLocation is ${pw}?`, "invalid");
        }
    } else if (key == "Backspace") {
        // delete
        if (pw.length == 0) {
            showAlert("Stop using sed.", "invalid");
            return;
        }
        setPendingWord(pw.substring(0, pw.length - 1));
    } else if (key.length == 1) {
        key = key.toUpperCase();
        if (!isValidChar(key)) return;
        setPendingWord(pw + key);
    }
}

const INSULTS = [
    "Help how do I use Ardour",
    "sudo rm -rf $(which sed)",
    "Terrible",
    "I smell a voepfxo",
    "Lyrics aged 3 months",
    "Drsk ass asemic thing",
    "Worse than M*necraft chat reporting",
    "Don’t fear the thorns",
    "Nice spam Uruwi",
    "Ŋ",
    "s-h-ut-u-p-fool",
    "Dw i ddim yn hoffi eich iaith wneud",
    "PC dedicated to running Vocaloid",
    "Eleanor Forte version 105",
    "Everything is green",
    "powe-sukijo",
    "class Nil is Cool",
    "GATs on f9i? 😳",
    "CeVIO on Linux pls",
    "Woodwind nationalism",
    "Do you like my sword",
    "Irish Shaders",
    "⑨⓪",
    "I’d rather look at AI art",
    "Nerunerunerunerunerunerunerune",
    "Conlanging is like M*necraft",
    "Uwu",
    "So symmetric and beautiful",
    "grep: Invalid back reference",
    "Nearsighted nekos",
    "1-800-FIX-YO-EARS",
    "The production value is great",
    "Makes you feel less bad about your own",
];

function submitWord(word, save) {
    let secret = currentGame.secretWord.key;
    let appraisal = appraise(word, secret);

    currentGame.priorGuesses.push(word);

    let renderedAppraisal = renderWordWithAppraisal(word, appraisal);
    playArea.appendChild(renderedAppraisal);

    for (let i = 0; i < word.length; ++i) {
        let letter = word.charAt(i);
        let app = appraisal.result[i];
        let kbButton = keyboardButtons[letter];
        switch (app) {
            case 0: {
                if (kbButton.className == "") {
                    kbButton.className = "gray";
                }
                break;
            }
            case 1: {
                if (kbButton.className == "") {
                    kbButton.className = "yellow";
                }
                break;
            }
            case 2: case 3: {
                kbButton.className = "green";
                break;
            }
        }

    }

    if (word == secret) {
        showAlert(INSULTS[Math.floor(INSULTS.length * Math.random())], "information");
        let k = currentGame.priorGuesses.length - 1;
        currentGame.status = 'won';
        let st = statistics;
        if (save) {
            st.played++;
            st.moves[k]++;
            st.totalMoves += k + 1;
            for (let g of currentGame.priorGuesses) {
                st.totalLetters += g.length;
            }
        }
        window.setTimeout(() => openResultsScreen(st), 1000);
    }

    setPendingWord("");


    if (save) {
        localStorage.setItem("currentGame", JSON.stringify(currentGame));
        localStorage.setItem("statistics", JSON.stringify(statistics));
    }
}

const AP_MISS = 0;
const AP_MISORDER = 1;
const AP_START = 2;
const AP_CONTINUE = 3;

function appraise(guess, secret) {
    let m = guess.length;
    let n = secret.length;

    let result = Array(m);
    result.fill(AP_MISS);
    let used = Array(n);
    used.fill(false);

    let table = Array(m + 1);
    for (let i = 0; i < m + 1; ++i) {
        table[i] = Array(n + 1);
    }

    table[0].fill(0);
    for (let i = 1; i < m + 1; ++i) {
        table[i][0] = 0;
        for (let j = 1; j < n + 1; ++j) {
            table[i][j] =
                (guess.charCodeAt(i - 1) == secret.charCodeAt(j - 1)) ?
                    table[i - 1][j - 1] + 1 :
                    Math.max(table[i][j - 1], table[i - 1][j]);
        }
    }

    {
        let i = m;
        let j = n;
        let continued = false;

        while (i > 0 && j > 0) {
            if (guess.charCodeAt(i - 1) == secret.charCodeAt(j - 1)) {
                // console.log(`${i} ${j}`);
                result[i - 1] = continued ? AP_CONTINUE : AP_START;
                used[j - 1] = true;
                i -= 1;
                j -= 1;
                continued = true;
            } else if (table[i][j - 1] > table[i - 1][j]) {
                j -= 1;
                continued = false;
            } else {
                i -= 1;
                continued = false;
            }
        }
    }

    let anchorStart = used[0];
    let anchorEnd = used[used.length - 1];

    let spareLetters = {};
    for (let j = 0; j < n; ++j) {
        if (!used[j]) {
            let c = secret.charAt(j);
            if (!spareLetters[c]) {
                spareLetters[c] = [j];
            } else {
                spareLetters[c].push(j);
            }
        }
    }

    for (let i = 0; i < m; ++i) {
        if (result[i] == AP_MISS) {
            let c = guess.charAt(i);
            if (spareLetters[c] && spareLetters[c].length) {
                spareLetters[c].shift();
                result[i] = AP_MISORDER;
            }
        }
    }

    return {
        result,
        anchorStart,
        anchorEnd,
    };
}

function appraisalRuns(appraisal) {
    let m = appraisal.result.length;
    let runs = [];
    for (let i = 0; i < m; ++i) {
        if (appraisal.result[i] == AP_MISS || appraisal.result[i] == AP_MISORDER) {
            runs.push({
                type: appraisal.result[i],
                start: i,
                end: i + 1,
                anchorStart: false,
                anchorEnd: false,
            });
        } else {
            let j = i;
            while (j < m && appraisal.result[j] != AP_START) j++;
            runs.push({
                type: AP_START,
                start: i,
                end: j + 1,
                anchorStart: i == 0 && appraisal.anchorStart,
                anchorEnd: j + 1 == m && appraisal.anchorEnd,
            });
            i = j;
        }
    }

    return runs;
}

function renderWordWithAppraisal(word, appraisal) {
    let row = document.createElement("div");
    row.setAttribute("class", "gameRow");
    for (let entry of appraisalRuns(appraisal)) {
        let cell = document.createElement("span");
        let classes = "gameCell " + ["gray", "yellow", "green"][entry.type];
        if (entry.anchorStart) {
            classes += " anchorStart";
        }
        if (entry.anchorEnd) {
            classes += " anchorEnd";
        }
        cell.setAttribute("class", classes);
        cell.innerText = word.substring(entry.start, entry.end);
        row.appendChild(cell);
    }
    return row;
}

function showAlert(text, cls) {
    function f(visibility) {
        if (visibility < 0) {
            alertBox.style = "display:none";
            return;
        }
        let v = Math.min(1, visibility);
        let w = 1 - (1 - v) * (1 - v);
        alertBox.style = `opacity:${w};top:${5 * w}em`;
        window.requestAnimationFrame(() => f(visibility - 0.01));
    }
    alertBox.innerText = text;
    alertBox.className = cls || "information";
    window.requestAnimationFrame(() => f(1.5));
}

function formatTimeDiff(ms) {
    let totalSecs = Math.floor(ms / 1000);
    let hours = Math.floor(totalSecs / 3600);
    let mins = Math.floor(totalSecs / 60) % 60;
    let secs = totalSecs % 60;
    return ('0' + hours).slice(-2) + ":" + ('0' + mins).slice(-2) + ":" + ('0' + secs).slice(-2);
}

function openResultsScreen(stats) {
    resultsScreen.style = "";
    resultsScreenTitle.innerText = `You won Fickle #${currentGame.gameNumber}!`;
    resultsScreenSubtitle.innerText = `It only took you ${currentGame.priorGuesses.length} guessim`;
    resultsPlayed.innerText = stats.played;

    resultsGuesses.innerText = currentGame.priorGuesses.length;
    let numLetters = 0;
    for (let word of currentGame.priorGuesses) {
        numLetters += word.length;
    }
    resultsLetters.innerText = numLetters;

    resultsAvgGuesses.innerText = stats.totalMoves / stats.played;
    resultsAvgLetters.innerText = stats.totalLetters / stats.played;

    resultsSongTitle.innerText = currentGame.secretWord.name;
    resultsSongTitle.href = `https://vocadb.net/S/${currentGame.secretWord.id}`;
    resultsSongArtist.innerText = currentGame.secretWord.artist;

    // set up clock
    function f() {
        if (resultsScreen.style == "display:none") {
            window.clearInterval(timeInterval);
        }
        let diff = Math.max(0, nextGameStart - Date.now());
        resultsTimeToNext.innerText = formatTimeDiff(diff);
    }
    let nextGameStart = timeFromGameNumber(currentGame.gameNumber + 1);
    let timeInterval = window.setInterval(f, 1000);
    f();
}

function closeResults() {
    resultsScreen.style = "display:none";
}

function share() {
    let numLetters = 0;
    for (let word of currentGame.priorGuesses) {
        numLetters += word.length;
    }
    let shareString = `Fickle ${currentGame.gameNumber}\n${currentGame.priorGuesses.length} words / ${numLetters} letters`;
    for (let row of currentGame.priorGuesses) {
        shareString += "\n";
        let appraisal = appraise(row, currentGame.secretWord.key);
        for (let rect of appraisal.result) {
            switch (rect) {
                case 0: shareString += "⬜"; break;
                case 1: shareString += "🟨"; break;
                case 2: case 3: shareString += "🟦"; break;
            }
        }
    }
    navigator.clipboard.writeText(shareString);
    showAlert("Copied to clipboard", "information");
}

function showHelp() {
    helpScreen.style = "";
}

function hideHelp() {
    helpScreen.style = "display:none";
}

function showUpdates() {
    updateScreen.style = "";
}

function hideUpdates() {
    updateScreen.style = "display:none";
}


